#!/bin/bash

# BWA align app wrapper script


###############################################################################
#### Helper Functions ####
###############################################################################

## ****************************************************************************
## Usage description should match command line arguments defined below
usage () {
    echo "Usage: $(basename "$0")"
    echo "  --input => Sequence FASTQ File"
    echo "  --pair => Paired-End Sequence FASTQ File"
    echo "  --reference_index => Reference Index Directory"
    echo "  --threads => CPU Threads"
    echo "  --output => Output Directory"
    echo "  --exec_method => Execution method (singularity, auto)"
    echo "  --exec_init => Execution initialization command(s)"
    echo "  --help => Display this help message"
}
## ****************************************************************************

# report error code for command
safeRunCommand() {
    cmd="$@"
    eval "$cmd; "'PIPESTAT=("${PIPESTATUS[@]}")'
    for i in ${!PIPESTAT[@]}; do
        if [ ${PIPESTAT[$i]} -ne 0 ]; then
            echo "Error when executing command #${i}: '${cmd}'"
            exit ${PIPESTAT[$i]}
        fi
    done
}

# print message and exit
fail() {
    msg="$@"
    echo "${msg}"
    usage
    exit 1
}

# always report exit code
reportExit() {
    rv=$?
    echo "Exit code: ${rv}"
    exit $rv
}

trap "reportExit" EXIT

# check if string contains another string
contains() {
    string="$1"
    substring="$2"

    if test "${string#*$substring}" != "$string"; then
        return 0    # $substring is not in $string
    else
        return 1    # $substring is in $string
    fi
}



###############################################################################
## SCRIPT_DIR: directory of current script, depends on execution
## environment, which may be detectable using environment variables
###############################################################################
if [ -z "${AGAVE_JOB_ID}" ]; then
    # not an agave job
    SCRIPT_DIR=$(dirname "$(readlink -f "$0")")
else
    echo "Agave job detected"
    SCRIPT_DIR=$(pwd)
fi
## ****************************************************************************



###############################################################################
#### Parse Command-Line Arguments ####
###############################################################################

getopt --test > /dev/null
if [ $? -ne 4 ]; then
    echo "`getopt --test` failed in this environment."
    exit 1
fi

## ****************************************************************************
## Command line options should match usage description
OPTIONS=
LONGOPTIONS=help,exec_method:,exec_init:,input:,pair:,reference_index:,threads:,output:,
## ****************************************************************************

# -temporarily store output to be able to check for errors
# -e.g. use "--options" parameter by name to activate quoting/enhanced mode
# -pass arguments only via   -- "$@"   to separate them correctly
PARSED=$(\
    getopt --options=$OPTIONS --longoptions=$LONGOPTIONS --name "$0" -- "$@"\
)
if [ $? -ne 0 ]; then
    # e.g. $? == 1
    #  then getopt has complained about wrong arguments to stdout
    usage
    exit 2
fi

# read getopt's output this way to handle the quoting right:
eval set -- "$PARSED"

## ****************************************************************************
## Set any defaults for command line options
THREADS="2"
EXEC_METHOD="auto"
EXEC_INIT=":"
## ****************************************************************************

## ****************************************************************************
## Handle each command line option. Lower-case variables, e.g., ${file}, only
## exist if they are set as environment variables before script execution.
## Environment variables are used by Agave. If the environment variable is not
## set, the Upper-case variable, e.g., ${FILE}, is assigned from the command
## line parameter.
while true; do
    case "$1" in
        --help)
            usage
            exit 0
            ;;
        --input)
            if [ -z "${input}" ]; then
                INPUT=$2
            else
                INPUT="${input}"
            fi
            shift 2
            ;;
        --pair)
            if [ -z "${pair}" ]; then
                PAIR=$2
            else
                PAIR="${pair}"
            fi
            shift 2
            ;;
        --reference_index)
            if [ -z "${reference_index}" ]; then
                REFERENCE_INDEX=$2
            else
                REFERENCE_INDEX="${reference_index}"
            fi
            shift 2
            ;;
        --threads)
            if [ -z "${threads}" ]; then
                THREADS=$2
            else
                THREADS="${threads}"
            fi
            shift 2
            ;;
        --output)
            if [ -z "${output}" ]; then
                OUTPUT=$2
            else
                OUTPUT="${output}"
            fi
            shift 2
            ;;
        --exec_method)
            if [ -z "${exec_method}" ]; then
                EXEC_METHOD=$2
            else
                EXEC_METHOD="${exec_method}"
            fi
            shift 2
            ;;
        --exec_init)
            if [ -z "${exec_init}" ]; then
                EXEC_INIT=$2
            else
                EXEC_INIT="${exec_init}"
            fi
            shift 2
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Invalid option"
            usage
            exit 3
            ;;
    esac
done
## ****************************************************************************

## ****************************************************************************
## Log any variables passed as inputs
echo "Input: ${INPUT}"
echo "Pair: ${PAIR}"
echo "Reference_index: ${REFERENCE_INDEX}"
echo "Threads: ${THREADS}"
echo "Output: ${OUTPUT}"
echo "Execution Method: ${EXEC_METHOD}"
echo "Execution Initialization: ${EXEC_INIT}"
## ****************************************************************************



###############################################################################
#### Validate and Set Variables ####
###############################################################################

## ****************************************************************************
## Add app-specific logic for handling and parsing inputs and parameters

# INPUT input

if [ -z "${INPUT}" ]; then
    echo "Sequence FASTQ File required"
    echo
    usage
    exit 1
fi
# make sure INPUT is staged
count=0
while [ ! -f "${INPUT}" ]
do
    echo "${INPUT} not staged, waiting..."
    sleep 1
    count=$((count+1))
    if [ $count == 10 ]; then break; fi
done
if [ ! -f "${INPUT}" ]; then
    echo "Sequence FASTQ File not found: ${INPUT}"
    exit 1
fi
INPUT_FULL=$(readlink -f "${INPUT}")
INPUT_DIR=$(dirname "${INPUT_FULL}")
INPUT_BASE=$(basename "${INPUT_FULL}")


# PAIR input

if [ -n "${PAIR}" ]; then
    # make sure ${PAIR} is staged
    count=0
    while [ ! -f "${PAIR}" ]
    do
        echo "${PAIR} not staged, waiting..."
        sleep 1
        count=$((count+1))
        if [ $count == 10 ]; then break; fi
    done
    if [ ! -f "${PAIR}" ]; then
        echo "Paired-End Sequence FASTQ File not found: ${PAIR}"
        exit 1
    fi
    PAIR_FULL=$(readlink -f "${PAIR}")
    PAIR_DIR=$(dirname "${PAIR_FULL}")
    PAIR_BASE=$(basename "${PAIR_FULL}")
fi



# REFERENCE_INDEX input

if [ -z "${REFERENCE_INDEX}" ]; then
    echo "Reference Index Directory required"
    echo
    usage
    exit 1
fi
# make sure REFERENCE_INDEX is staged
count=0
while [ ! -d "${REFERENCE_INDEX}" ]
do
    echo "${REFERENCE_INDEX} not staged, waiting..."
    sleep 1
    count=$((count+1))
    if [ $count == 10 ]; then break; fi
done
if [ ! -d "${REFERENCE_INDEX}" ]; then
    echo "Reference Index Directory not found: ${REFERENCE_INDEX}"
    exit 1
fi
REFERENCE_INDEX_FULL=$(readlink -f "${REFERENCE_INDEX}")
REFERENCE_INDEX_DIR=$(dirname "${REFERENCE_INDEX_FULL}")
REFERENCE_INDEX_BASE=$(basename "${REFERENCE_INDEX_FULL}")
MNT=""; ARG=""; CMD0="BWT_FILE=$(ls ${REFERENCE_INDEX_FULL} | grep '.bwt$') ${ARG}"; CMD="${CMD0}"; echo "CMD=${CMD}"; safeRunCommand "${CMD}"; 
if [ -z "${BWT_FILE}" ]; then MNT=""; ARG=""; CMD0="fail 'Invalid BWA reference index' ${ARG}"; CMD="${CMD0}"; echo "CMD=${CMD}"; safeRunCommand "${CMD}"; fi; 
MNT=""; ARG=""; CMD0="REFERENCE_INDEX_PREFIX=\"${BWT_FILE%.*}\" ${ARG}"; CMD="${CMD0}"; echo "CMD=${CMD}"; safeRunCommand "${CMD}"; 



# THREADS parameter
if [ -n "${THREADS}" ]; then
    :
else
    :
fi


# OUTPUT parameter
if [ -n "${OUTPUT}" ]; then
    :
    OUTPUT_FULL=$(readlink -f "${OUTPUT}")
    OUTPUT_DIR=$(dirname "${OUTPUT_FULL}")
    OUTPUT_BASE=$(basename "${OUTPUT_FULL}")
    LOG_FULL="${OUTPUT_DIR}/_log"
    TMP_FULL="${OUTPUT_DIR}/_tmp"
else
    :
    echo "Output Directory required"
    echo
    usage
    exit 1
fi

## ****************************************************************************

## EXEC_METHOD: execution method
## Suggested possible options:
##   auto: automatically determine execution method
##   singularity: singularity image packaged with the app
##   docker: docker containers from docker-hub
##   environment: binaries available in environment path

## ****************************************************************************
## List supported execution methods for this app (space delimited)
exec_methods="singularity auto"
## ****************************************************************************

## ****************************************************************************
# make sure the specified execution method is included in list
if ! contains " ${exec_methods} " " ${EXEC_METHOD} "; then
    echo "Invalid execution method: ${EXEC_METHOD}"
    echo
    usage
    exit 1
fi
## ****************************************************************************



###############################################################################
#### App Execution Initialization ####
###############################################################################

## ****************************************************************************
## Execute any "init" commands passed to the GeneFlow CLI
CMD="${EXEC_INIT}"
echo "CMD=${CMD}"
safeRunCommand "${CMD}"
## ****************************************************************************



###############################################################################
#### Auto-Detect Execution Method ####
###############################################################################

# assign to new variable in order to auto-detect after Agave
# substitution of EXEC_METHOD
AUTO_EXEC=${EXEC_METHOD}
## ****************************************************************************
## Add app-specific paths to detect the execution method.
if [ "${EXEC_METHOD}" = "auto" ]; then
    # detect execution method
    if command -v singularity >/dev/null 2>&1; then
        AUTO_EXEC=singularity
    else
        echo "Valid execution method not detected"
        echo
        usage
        exit 1
    fi
    echo "Detected Execution Method: ${AUTO_EXEC}"
fi
## ****************************************************************************



###############################################################################
#### App Execution Preparation, Common to all Exec Methods ####
###############################################################################

## ****************************************************************************
## Add logic to prepare environment for execution
MNT=""; ARG=""; CMD0="mkdir -p ${OUTPUT_FULL} ${ARG}"; CMD="${CMD0}"; echo "CMD=${CMD}"; safeRunCommand "${CMD}"; 
MNT=""; ARG=""; CMD0="mkdir -p ${LOG_FULL} ${ARG}"; CMD="${CMD0}"; echo "CMD=${CMD}"; safeRunCommand "${CMD}"; 
## ****************************************************************************



###############################################################################
#### App Execution, Specific to each Exec Method ####
###############################################################################

## ****************************************************************************
## Add logic to execute app
## There should be one case statement for each item in $exec_methods
case "${AUTO_EXEC}" in
    singularity)
        CMD=""; MNT=""; ARG=""; ARG="${ARG} -t"; ARG="${ARG} \"${THREADS}\""; MNT="${MNT} -B "; MNT="${MNT}\"${REFERENCE_INDEX_DIR}:/data2\""; ARG="${ARG} \"/data2/${REFERENCE_INDEX_BASE}/${REFERENCE_INDEX_PREFIX}\""; MNT="${MNT} -B "; MNT="${MNT}\"${INPUT_DIR}:/data3\""; ARG="${ARG} \"/data3/${INPUT_BASE}\""; if [ -n "${PAIR}" ]; then MNT="${MNT} -B "; MNT="${MNT}\"${PAIR_DIR}:/data4\""; ARG="${ARG} \"/data4/${PAIR_BASE}\""; fi; CMD1="singularity -s exec ${MNT} docker://quay.io/biocontainers/bwa:0.7.17--hed695b0_7 bwa mem ${ARG}"; CMD1="${CMD1} 2>\"${LOG_FULL}/${OUTPUT_BASE}-bwa-mem.stderr\""; CMD="${CMD}${CMD1}"; MNT=""; ARG=""; CMD2="singularity -s exec ${MNT} docker://quay.io/biocontainers/samtools:1.10--h9402c20_1 samtools view -b - ${ARG}"; CMD2="${CMD2} >\"${OUTPUT_FULL}/${OUTPUT_BASE}.bam\""; CMD2="${CMD2} 2>\"${LOG_FULL}/${OUTPUT_BASE}-samtools-view.stderr\""; CMD="${CMD}|${CMD2}"; echo "CMD=${CMD}"; safeRunCommand "${CMD}"; 
        ;;
esac
## ****************************************************************************



###############################################################################
#### Cleanup, Common to All Exec Methods ####
###############################################################################

## ****************************************************************************
## Add logic to cleanup execution artifacts, if necessary
## ****************************************************************************

