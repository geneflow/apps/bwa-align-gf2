BWA Align GeneFlow App
======================

Version: 0.2

This GeneFlow app wraps bwa and samtools to generate a standard bam mapping.

Inputs
------

1. input: Sequence FASTQ File - A sequence file in the FASTQ file format.

2. pair: Paired-End Sequence FASTQ File - A paired-end sequence file in the FASTQ file format. The default value for this input is "null", and can be left blank for single-end sequence alignment.

3. reference: BWA Reference Index - A directory that contains a BWA reference index. This index includes multiple files, but must only contain one reference index. 

Parameters
----------

1. threads: CPU Threads - The number of CPU threads to use for sequence alignment. Default: 2.
 
2. output: Output Directory - The name of the output directory that will contain the BAM file.

